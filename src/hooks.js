const createFailureLocalizationHooks  = require( "cbfl").createFailureLocalizationHooks;

console.log(process.env.GITLAB_API_TOKEN)
const options = {
    mochaCommand: "TS_NODE_FILES=true node ./node_modules/mocha/bin/mocha src/**/*.spec.ts  -r ts-node/register -t 200000",
    gitlabApiToken: process.env.GITLAB_API_TOKEN,
    targetBranch: "origin/master"
};
const hooks = createFailureLocalizationHooks(options);
exports.mochaHooks = {
    async beforeAll() {
        await hooks.beforeAll()
    },
    async afterAll() {
        await hooks.afterAll()
    },
    async afterEach() {
        hooks.afterEach(this.currentTest)
    }
};