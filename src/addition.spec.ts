import { equal } from "assert";
import {add, sub} from "./addition";

describe("Calculator ", () => {
  it("should add two numbers", () => {
    equal(add(2, 3), 5);
  });
  it("should subtract two numbers", () => {
    equal(sub(2, 3), -1);
  });
});
