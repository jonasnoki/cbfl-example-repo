const { traverseHistory } = require("cbfl");

traverseHistory(
    "TS_NODE_FILES=true npm run test",
    __dirname + "/src/hooks.js"
);
